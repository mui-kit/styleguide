import React from 'react'
import { hot } from 'react-hot-loader/root'
import Root from './components/layout'
import Colors from './components/sections/colors'
import Tipography from './components/sections/tipography'
import Iconography from './components/sections/iconography'
import Buttons from './components/sections/buttons'
import Forms from './components/sections/forms'

import Typography from '@material-ui/core/Typography'

const App = () => (
	<Root>
		<Colors />
		<Tipography />
		<Iconography />
		<Buttons />
		<Forms />
	</Root>
)

export default hot(App)
