import React from 'react'

import Typography from '@material-ui/core/Typography'

const Tipography = () => (
	<>
		<Typography variant="h2">Typography</Typography>

		<Typography variant="h1">Headline 1 variant</Typography>
		<Typography variant="h2">Headline 2 variant</Typography>
		<Typography variant="h3">Headline 3 variant</Typography>
		<Typography variant="h4">Headline 4 variant</Typography>
		<Typography variant="h5">Headline 5 variant</Typography>
		<Typography variant="h6">Headline 6 variant</Typography>
		<Typography variant="subtitle1">Subtitle 1 variant</Typography>
		<Typography variant="subtitle2">Subtitle 1 variant</Typography>
		<Typography variant="body1">Body 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur unde suscipit, quam beatae rerum inventore consectetur, neque doloribus, cupiditate numquam dignissimos laborum fugiat deleniti? Eum quasi quidem quibusdam.</Typography>
		<Typography variant="body2">Body 2. Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate aliquid ad quas sunt voluptatum officia dolorum cumque, possimus nihil molestias sapiente necessitatibus dolor saepe inventore, soluta id accusantium voluptas beatae.</Typography>
		<Typography variant="caption">Caption text variant</Typography>
		<Typography variant="button">BUTTON TEXT variant</Typography>
		<Typography variant="overline">OVERLINE TEXT variant</Typography>
	</>
)

export default Tipography
