import { makeStyles } from '@material-ui/styles'

export default makeStyles(({ palette, spacing }) => ({
	root: {
		padding: spacing(2)
	},
	primary: {
		background: palette.primary.main,
	},
	primaryLight: {
		background: palette.primary.light,
	},
	primaryDark: {
		background: palette.primary.dark,
	},
	secondary: {
		background: palette.secondary.main,
	},
	secondaryLight: {
		background: palette.secondary.light,
	},
	secondaryDark: {
		background: palette.secondary.dark,
	}
}))
