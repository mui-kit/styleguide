import React from 'react'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'

import Color from './color'

import Typography from '@material-ui/core/Typography'
import useStyles from './Colors.styles'

const Colors = () => {
	const classes = useStyles()

	return (
		<Grid container spacing={3}>
			<Grid item xs={12}>
				<Typography variant="h2">Colors</Typography>
			</Grid>
			<Color className={`${classes.root} ${classes.primary}`} text="Primary" />
			<Color className={`${classes.root} ${classes.primaryLight}`} text="Primary Light" />
			<Color className={`${classes.root} ${classes.primaryDark}`} text="Primary Dark" />
			<Color className={`${classes.root} ${classes.secondary}`} text="Secondary" />
			<Color className={`${classes.root} ${classes.secondaryLight}`} text="Secondary Light" />
			<Color className={`${classes.root} ${classes.secondaryDark}`} text="Secondary Dark" />
		</Grid>
	)
}

export default Colors
