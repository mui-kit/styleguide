import React from 'react'

import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

const Color = ({
	className,
	text
}) => (
		<>
			<Grid item xs={12} sm={6}>
				<Paper className={className}>
				</Paper>
			</Grid>
			<Grid item xs={12} sm={6}>
				<Typography>{text}</Typography>
			</Grid>
		</>
	)

export default Color