import React from 'react'

import Container from '@material-ui/core/Container'
import Box from '@material-ui/core/Box'

import useStyles from './Content.styles'

function Content({ children }) {
	const classes = useStyles()

	return (
		<Container component="main">
			<Box className={classes.header} />

			{children}
		</Container>
	)
}

export { Content }
