import { makeStyles } from '@material-ui/styles'

export default makeStyles(({ mixins }) => ({
	'@global': {
		'#root': {
			display: 'flex'
		}
	},
	header: {
		...mixins.toolbar
	}
}))
