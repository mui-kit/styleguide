import { Header, Content, Nav } from '@components/layout'
import CssBaseline from '@material-ui/core/CssBaseline'
import { ThemeProvider } from '@material-ui/styles'
import { theme } from '@mui-kit/styleguide'
import React from 'react'

const Root = ({ children }) => (
	<ThemeProvider theme={theme}>
		<CssBaseline />

		<Header />
		<Nav />
		<Content>
			{children}
		</Content>
	</ThemeProvider>
)

export default Root
