import { makeStyles } from '@material-ui/styles'

export default makeStyles(({ zIndex }) => ({
	appBar: {
		zIndex: zIndex.drawer + 1
	},
	title: {
		flexGrow: 1,
	}
}))
