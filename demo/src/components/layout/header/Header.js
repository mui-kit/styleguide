import React from 'react'

import Typography from '@material-ui/core/Typography'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'

import useStyles from './Header.styles'

const Header = () => {
	const classes = useStyles()

	return (
		<AppBar
			className={classes.appBar}
			position="fixed"
		>
			<Toolbar>
				<Typography
					variant="h6"
					className={classes.title}
					noWrap
				>
					Styleguide Overview
				</Typography>
			</Toolbar>
		</AppBar >
	)
}

export { Header }
