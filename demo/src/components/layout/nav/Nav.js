import React from 'react'

import Box from '@material-ui/core/Box'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Drawer from '@material-ui/core/Drawer'

import useStyles from './Nav.styles'

const Nav = () => {
	const classes = useStyles()

	return (
		<Drawer
			variant="permanent"
			className={classes.drawer}
			classes={{
				paper: classes.drawer
			}}
		>
			<Box className={classes.header} />
			<List>
				<ListItem>
					<ListItemText primary="Colors" />
				</ListItem>

				<ListItem>
					<ListItemText primary="Typography" />
				</ListItem>

				<ListItem>
					<ListItemText primary="Iconography" />
				</ListItem>

				<ListItem>
					<ListItemText primary="Buttons" />
				</ListItem>

				<ListItem>
					<ListItemText primary="Forms" />
				</ListItem>
			</List>
		</Drawer>
	)
}

export { Nav }
