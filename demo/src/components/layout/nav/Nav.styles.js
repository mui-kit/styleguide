import { makeStyles } from '@material-ui/core/styles'
import { drawerWidth } from '@components/layout/properties.json';

export default makeStyles(({ mixins }) => ({
	header: {
		...mixins.toolbar
	},
	drawer: {
		width: drawerWidth,
		whiteSpace: 'nowrap',
		overflowX: 'hidden'
	}
}))
