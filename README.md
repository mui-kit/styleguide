# [Styleguide][demo-url]

> Shared theme for [Material-UI](https://material-ui.com) based React Apps

[![npm][npm]][npm-url]
[![downloads][downloads]][downloads-url]

**[Demo][demo-url]** | **[Issues][issues-url]**

## Install

Make sure you have the latest version of [node](https://nodejs.org/) and [npm](https://www.npmjs.com) installed.

```bash
npm install --save @mui-kit/styleguide
```

`styleguide` requires [@material-ui/core][mui-core-url] as a peer dependency.

```bash
npm install --save @material-ui/core
```

## Usage

```jsx
import React from "react"
import { theme } from "@mui-kit/styleguide"
import { ThemeProvider } from '@material-ui/styles'

const Example = ({ children }) => (
  <ThemeProvider theme={theme}>
    {children}
  </ThemeProvider>
)
```

## Contributing

[There are plenty of opportunities to get involved](https://gitlab.com/mui-kit/styleguide/issues). Pick an outstanding task, let us know what you are working on and fire away with any questions.

The package is made up of 2 main folders:

- /src contains the React Styleguide
- /demo is our demo website

To setup and run a local copy:

1.  Clone this repo with `git clone https://gitlab.com/mui-kit/styleguide.git`
2.  Run `npm install` in the root folder
3.  Run `npm install` in the demo folder
4.  In separate terminal windows, run `npm start` in the root and demo folders.

You should now be up and running with live browser reloading of the demo website while you work on the Styleguide in the `/src` folder.

When you're done working on your changes, submit a PR with the details and include a screenshot if you've changed anything visually.

[npm]: https://img.shields.io/npm/v/@mui-kit/styleguide.svg
[npm-url]: https://npmjs.com/package/@mui-kit/styleguide
[downloads]: https://img.shields.io/npm/dt/@mui-kit/styleguide.svg
[downloads-url]: https://npmjs.com/package/@mui-kit/styleguide
[demo-url]: https://mui-kit.gitlab.io/styleguide
[issues-url]: https://gitlab.com/mui-kit/styleguide/issues
[react-url]: https://www.npmjs.com/package/react
[react-dom-url]: https://www.npmjs.com/package/react-dom
[mui-core-url]: https://www.npmjs.com/package/@material-ui/core
[mui-icons-url]: https://www.npmjs.com/package/@material-ui/icons
