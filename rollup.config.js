import resolve from 'rollup-plugin-node-resolve'
import pkg from './package.json'

const externalDeps = ['@material-ui']
const external = uri => externalDeps.some(dep => uri.startsWith(dep))

export default [
	{
		input: 'src/index.js',
		external,
		output: {
			file: pkg.main,
			format: 'cjs'
		},
		plugins: [resolve()]
	},
	{
		input: 'src/index.js',
		external,
		output: {
			file: pkg.module,
			format: 'es'
		},
		plugins: [resolve()]
	}
]
